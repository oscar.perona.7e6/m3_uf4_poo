

class Pasta(val name:String, val peso:String, val precio:Double, val calorias:String)

fun main(){

    val croissant = Pasta("Croissant", "65g", 0.85, "406 kcal")
    val ensaimada = Pasta("Ensaimada", "80g", 1.20, "500 kcal")
    val donut = Pasta("Donut", "60g", 1.50, "621 kcal")

}