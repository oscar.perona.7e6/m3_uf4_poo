
class Beguda(val name:String, var price:Double, val increment:Boolean){
    fun incrementPrice(){
        if (increment) price += 10*price/100
    }
}

fun main(){

    val croissant = Pasta("Croissant", "65g", 0.85, "406 kcal")
    val ensaimada = Pasta("Ensaimada", "80g", 1.20, "500 kcal")
    val donut = Pasta("Donut", "60g", 1.50, "621 kcal")

    val agua = Beguda("Agua", 1.00, false)
    val cafe = Beguda("Café tallat", 1.35 , false)
    val te = Beguda("Té vermell", 1.50, false)
    val cola = Beguda("Cola", 1.65, true)

    agua.incrementPrice()
    cafe.incrementPrice()
    te.incrementPrice()
    cola.incrementPrice()

    println("Pastas")
    println("${croissant.name}, ${croissant.peso}, ${croissant.precio}, ${croissant.calorias}")
    println("${ensaimada.name}, ${ensaimada.peso}, ${ensaimada.precio}, ${ensaimada.calorias}")
    println("${donut.name}, ${donut.peso}, ${donut.precio}, ${donut.calorias}")
    println()
    println("Bebidas")
    println("${agua.name}, ${agua.price}, ${agua.increment}")
    println("${cafe.name}, ${cafe.price}, ${cafe.increment}")
    println("${te.name}, ${te.price}, ${te.increment}")
    println("${cola.name}, ${cola.price}, ${cola.increment}")
}