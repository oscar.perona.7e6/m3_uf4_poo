class MachanicalArm(){
    var turnedOn = false
    var opertura = 0
    var heigh = 0

    init {
        turnedOn = false
        opertura = 0
        heigh = 0
    }
    fun toggle(){
        if(turnedOn) turnedOn = false
        else turnedOn = true
    }
    fun updateAltitude(altitude:Int){
        heigh += altitude
        if (heigh > 30) heigh -= 30

    }
    fun updateAngle(angle:Int){
        opertura += angle
        if (opertura > 360) opertura -=360
    }
}

fun main(){
    val robot = MachanicalArm()
    imputOrders(robot)
}

fun imputOrders(robot: MachanicalArm){
    robot.toggle()
    printStatus(robot)
    robot.updateAltitude(3)
    printStatus(robot)
    robot.updateAngle(180)
    printStatus(robot)
    robot.updateAltitude(-3)
    printStatus(robot)
    robot.updateAngle(-180)
    printStatus(robot)
    robot.updateAltitude(3)
    printStatus(robot)
    robot.toggle()
    printStatus(robot)
}

fun printStatus(robot: MachanicalArm){
    println("MechanicalArm{openAngle=${robot.opertura}, altitude=${robot.heigh}, turnedOn=${robot.turnedOn}}")
}