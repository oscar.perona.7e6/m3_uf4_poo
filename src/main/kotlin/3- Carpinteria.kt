import java.util.*

class Pedido(val tipus:String, val preuUnitari:Int, val llargada:Int, val amplada:Int){
    fun updatePrice(){
        if (tipus == "Taulell") finalPrice += (llargada*amplada)*preuUnitari
        else finalPrice += preuUnitari*llargada
    }
}

var finalPrice = 0

fun main(){

    val scanner = Scanner(System.`in`)
    val elemntsNumber = scanner.nextInt()

    repeat(elemntsNumber){
        val tipus = scanner.next()
        if (tipus == "Taulell"){
            val pedido = Pedido(tipus, scanner.nextInt(), scanner.nextInt(), scanner.nextInt())
            pedido.updatePrice()
        }
        else{
            val pedido = Pedido(tipus, scanner.nextInt(), scanner.nextInt(), 0)
            pedido.updatePrice()
        }
    }

    println("El preu total és: ${finalPrice}€")
}