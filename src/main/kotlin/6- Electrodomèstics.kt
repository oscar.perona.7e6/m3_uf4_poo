open class Electrodomestics(val preuBase: Int, var color: String= "Blanc", val consum: String= "G", val pes: Int= 5){
    var preuFinal = 0
    init {
        preuFinal()
    }
    open fun preuFinal () {
        preuFinal += preuBase
        when (consum){
            "A" -> preuFinal += 35
            "B" -> preuFinal += 30
            "C" -> preuFinal += 25
            "D" -> preuFinal += 20
            "E" -> preuFinal += 15
            "F" -> preuFinal += 10
            "G" -> preuFinal += 0
        }
        when (pes){
            in 6..20 -> preuFinal += 20
            in 21..50 -> preuFinal += 50
            in 51..80 -> preuFinal += 80
            in 80..100000 -> preuFinal +=100
        }
    }
}

class Rentadora(preuBase: Int, color: String, consum: String, pes: Int,val carrega: Int) : Electrodomestics(preuBase, color, consum, pes){
    init {
        super.preuFinal
        preuFinal()
    }
    override fun preuFinal() {
        super.preuFinal += preuBase
        when (carrega){
            6 -> preuFinal += 55
            7 -> preuFinal += 55
            8 -> preuFinal += 70
            9 -> preuFinal += 85
            10 -> preuFinal += 100
        }
    }
}

class Televisio(preuBase: Int, color: String, consum: String, pes: Int,val tamany: Int) : Electrodomestics(preuBase, color, consum, pes){
    init {
        super.preuFinal
        preuFinal()
    }
    override fun preuFinal() {
        super.preuFinal += preuBase
        when (tamany){
            in 29..32 -> preuFinal += 50
            in 33..42 -> preuFinal += 100
            in 43..50 -> preuFinal += 150
            in 50..100000 -> preuFinal += 200
        }
    }
}

fun main(){
    val electrodomestic1=Electrodomestics(450,"platejat","A",53)
    val electrodomestic2=Electrodomestics(300,"color","B",74)
    val electrodomestic3=Electrodomestics(215,"blanc","C",33)
    val electrodomestic4=Electrodomestics(89,"blanc","D",28)
    val electrodomestic5=Electrodomestics(99,"color","E",81)
    val electrodomestic6=Electrodomestics(523,"platejat","F",115)
    val rentadora1=Rentadora(60,"color","A",80, 5)
    val rentadora2=Rentadora(72,"platejat","B",45, 9)
    val televisio1=Televisio(75,"color","D",33, 70)
    val televisio2=Televisio(60,"blanc","F",28, 28)
    val electrodomestics = mutableListOf(electrodomestic1,electrodomestic2,electrodomestic3,electrodomestic4,electrodomestic5,electrodomestic6,rentadora1,rentadora2,televisio1,televisio2)
    val preus = MutableList(6){0}
    for (electrodomestic in electrodomestics){
        if (electrodomestic is Rentadora) {
            preus[2] += electrodomestic.preuBase
            preus[3] += electrodomestic.preuFinal
        }
        else if(electrodomestic is Televisio){
                preus[4] += electrodomestic.preuBase
                preus[5] += electrodomestic.preuFinal
        }
        else{
            preus[0] += electrodomestic.preuBase
            preus[1] += electrodomestic.preuFinal
        }
    }
    println("Electrodomèstics: \n" +
            "\t -  Preu Base : ${preus[0]}€\n" +
            "\t -  Preu Final : ${preus[1]}€\n" +
            "Rentadores: \n" +
            "\t -  Preu Base : ${preus[2]}€\n" +
            "\t -  Preu Final : ${preus[3]}€\n" +
            "Televisors: \n" +
            "\t -  Preu Base : ${preus[4]}€\n" +
            "\t -  Preu Final : ${preus[5]}€\n")
}

